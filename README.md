![Alt text](LOGO_GROOVETECH-PRETO.png)

# **Desafio Automação**

---

## Objetivo

Avaliar as competências do candidato no entendimento e escrita de Cenários e na construção de testes automatizados, bem como no uso de boas práticas.

---

## Tecnologias

* *Para documentar os cenários especificados seguindo BDD*: **Cucumber**
* *Linguagem de programação*: **Ruby**, **Python**, **Kotlin**, **Java**
* *Bibliotecas*: as que achar necessário para execução dos testes

---

## Projeto
Criar um **único** projeto contendo:

* ***"Readme"*** com orientações de como instalar o ambiente e executar os testes
* A solução dos desafios propostos a seguir

---

## Desafio 01 - WebSite
1. Fazer um cadastro no site [http://automationpractice.com](http://automationpractice.com) e validar que foi realizado com sucesso.

**Diferencial**: Utilizar conceito de PageObjects, geração dinâmica de dados e escolha aleatória na seleção das opções (combo boxes, check boxes, etc)

---

## Desafio 02 - WebService
1. Enviar um **GET** para a API [http://swapi.co/api/films/](http://swapi.co/api/films/) e exibir o conteúdo do campo *“title”* de cada elemento da estrutura *“results”*. Validar o status code da resposta do serviço

**Diferencial**: Ao invés de todos os títulos, exibir somente dos filmes que tenham **George Lucas** como diretor e que **Rick McCallum** tenha participado como produtor

---

## Envio dos Desafios

Primeiramente, efetue um **fork** deste repositório. Desenvolva e faça os *commits* no seu **fork**. Quando terminar, envie um **Pull Request** através do **BitBucket**.

---

## Avaliação

Envie o projeto mesmo que não conclua 100%.

Será avaliado o entendimento do desafio, assim como foi estruturado o projeto, lógica, padrões, entre outros.
